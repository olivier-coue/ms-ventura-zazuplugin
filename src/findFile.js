var request = require('request')

module.exports = (pluginContext) => {
  return {
    respondsTo: (query, env = {}) => {
      return (query != null && query.trim() !== '')
    },
    search: (query, env = {}) => {
      return new Promise((resolve, reject) => {
        var folder = ''
        if (query.indexOf(':') >= 0) {
          var splitted = query.split(':')
          folder = splitted[0]
          query = splitted[1]
        }
        const isWindows = process.platform === 'win32'

        request('http://192.168.10.245:3000/files?q=' + query + '&f=' + folder, function (err, response, body) {
          if (err) {
            pluginContext.console.log('error', 'api error', err)
            resolve([{
              icon: 'fa-hand-spock-o',
              title: 'API Error',
              subtitle: 'check logs',
              value: '',
            }])
          } else {
            var files = JSON.parse(body)
            var res = []
            files.forEach(function (file) {
              var brianPathWindows = '\\\\192.168.10.245\\Brian\\Cabinet\\Bibliotheque' + file.path.replace('/usr/src/folder-to-watch-mounted-from-local', '').split('/').join('\\')
              var brianPathMac = '/Volumes/Brian/Cabinet/Bibliotheque' + file.path.replace('/usr/src/folder-to-watch-mounted-from-local', '')
              res.push({
                icon: `none`,
                iconUrl: `http://192.168.10.245:3000/images/${file._id}`,
                preview: '_',
                title: file.name,
                subtitle: file.path.replace('/usr/src/folder-to-watch-mounted-from-local', ''),
                value: isWindows ? brianPathWindows : brianPathMac,
                previewImage: `http://192.168.10.245:3000/images/${file._id}`,
                keywords: file.keywords,
                indexedFileId: file._id,
              })
            })
            resolve(res)
          }
        })
      })
    },
  }
}
